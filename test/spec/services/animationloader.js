'use strict';

describe('Service: AnimationLoader', function () {

  // load the service's module
  beforeEach(module('ngPageLoadingEffectsAppApp'));

  // instantiate service
  var AnimationLoader;
  beforeEach(inject(function (_AnimationLoader_) {
    AnimationLoader = _AnimationLoader_;
  }));

  it('should do something', function () {
    expect(!!AnimationLoader).toBe(true);
  });

});
