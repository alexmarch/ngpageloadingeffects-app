'use strict';

describe('Directive: pageAnimation', function () {

  // load the directive's module
  beforeEach(module('ngPageLoadingEffectsAppApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<page-animation></page-animation>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the pageAnimation directive');
  }));
});
