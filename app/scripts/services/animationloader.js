'use strict';

angular.module('ngPageLoadingEffectsAppApp')
  .factory('AnimationLoader', function () {
  	function AnimationLoader( $el, options ) {
  		this.$el = $el;
  		this.options = angular.extend( {}, this.options );
  		angular.extend( this.options, options );
  		this._init();
		}

		AnimationLoader.prototype.options = {
			speedIn : 500,
			easingIn : mina.linear
		};

		AnimationLoader.prototype._init = function() {
			var s = new Snap(this.$el.find( 'svg' )[0]);
			this.$loader = angular.element(this.$el.children()[0]);
			this.path = s.select('path');
			this.initialPath = this.path.attr('d');

			var openingStepsStr = this.$loader.attr( 'data-opening' );
			this.openingSteps = openingStepsStr ? openingStepsStr.split(';') : '';
			this.openingStepsTotal = openingStepsStr ? this.openingSteps.length : 0;

			if( this.openingStepsTotal === 0 ){
				return;
			}

			var closingStepsStr = this.$loader.attr( 'data-closing' ) ? this.$loader.attr( 'data-closing' ) : this.initialPath;
			this.closingSteps = closingStepsStr ? closingStepsStr.split(';') : '';
			this.closingStepsTotal = closingStepsStr ? this.closingSteps.length : 0;

			this.isAnimating = false;

			if( !this.options.speedOut ) {
				this.options.speedOut = this.options.speedIn;
			}
			if( !this.options.easingOut ) {
				this.options.easingOut = this.options.easingIn;
			}
	};

	AnimationLoader.prototype.show = function() {
		if( this.isAnimating ){
			return false;
		}
		this.isAnimating = true;
		// animate svg
		var self = this,

			onEndAnimation = function() {
				self.$loader.addClass('pageload-loading');
			};
		this._animateSVG( 'in', onEndAnimation );
		//this.$el.children()[0].addClass('show');
		//this.$el.children[0].text("Hello world");
		this.$loader.addClass('show');
		//console.log("Active show methos", this.$el.find('#loader');
	};

	AnimationLoader.prototype.hide = function() {
		var self = this;
		this.$loader.removeClass('pageload-loading');
		this._animateSVG( 'out', function() {
			// reset path
			self.path.attr( 'd', self.initialPath );
			self.$loader.removeClass('show');
			self.isAnimating = false;
		} );
	};

	AnimationLoader.prototype._animateSVG = function( dir, callback ) {
		var self = this,
			pos = 0,
			steps = dir === 'out' ? this.closingSteps : this.openingSteps,
			stepsTotal = dir === 'out' ? this.closingStepsTotal : this.openingStepsTotal,
			speed = dir === 'out' ? self.options.speedOut : self.options.speedIn,
			easing = dir === 'out' ? self.options.easingOut : self.options.easingIn,
			nextStep = function( pos ) {
				if( pos > stepsTotal - 1 ) {
					if( callback && typeof callback === 'function' ) {
						callback();
					}
					return;
				}
				self.path.animate( { 'path' : steps[pos] }, speed, easing, function() { nextStep(pos); } );
				pos++;
			};
		nextStep(pos);
	};
    return {
      Loader: AnimationLoader
    };
  });
