'use strict';

/**
 * @ngdoc directive
 * @name ngPageLoadingEffectsAppApp.directive:ngPage
 * @description
 * # ngPage
 */
angular.module('ngPageLoadingEffectsAppApp')
  .directive('ngPage', function(){
    function page(scope, $elem, attrs){
      if(attrs.hasOwnProperty('pageShow')){
        $elem.addClass('show');
      }else{
        $elem.addClass('hide');
      }
      $elem.addClass(attrs.ngPage);
      scope.pages.push($elem);
    }
    return {
      restrict: 'A',
      link: page
    };
  });
