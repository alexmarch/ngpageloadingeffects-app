'use strict';

/**
 * @ngdoc directive
 * @name ngPageLoadingEffectsAppApp.directive:pageAnimation
 * @description
 * # pageAnimation
 */
angular.module('ngPageLoadingEffectsAppApp')
  .directive('pageAnimation', function(){
    function animation(scope, $elem, attrs){
      scope.animations[attrs.animationId] = attrs;
      scope.animations[attrs.animationId].elem = $elem;
      var effect = scope.animationEffects[attrs.animation];
      var loaderTemplate = '<div id="loader" class="pageload-overlay" data-opening="'+effect.data_opening+'" data-closing="'+effect.data_closing+'">'+
        '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 80 60" preserveAspectRatio="none">'+
          '<path d="'+effect.path+'"/>'+
        '</svg>'+
      '</div>';
      $elem.html(loaderTemplate);
    }
    return {
      restrict: 'E',
      link: animation
    };
  });