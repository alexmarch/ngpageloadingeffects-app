'use strict';

/**
 * @ngdoc function
 * @name ngPageLoadingEffectsAppApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ngPageLoadingEffectsAppApp
 */
angular.module('ngPageLoadingEffectsAppApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
