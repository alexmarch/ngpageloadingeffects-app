'use strict';

/**
 * @ngdoc function
 * @name ngPageLoadingEffectsAppApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ngPageLoadingEffectsAppApp
 */
angular.module('ngPageLoadingEffectsAppApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
